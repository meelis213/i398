package stack;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StackTest {

    @Test
    public void newStackHasNoElements() {
        Stack stack = new Stack(100);

        assertThat(stack.getStackSize(), is(0));
    }

    @Test
    public void stackPush2Check0() {
        Stack stack = new Stack(100);

        stack.push(6);
        stack.push(9);

        assertThat(stack.getStackSize(), is(2));

    }

    @Test
    public void stackPush2Pop2Check0() {
        Stack stack = new Stack(1000);

        stack.push(15);
        stack.push(20);
        stack.pop();
        stack.pop();

        assertThat(stack.getStackSize(), is (0));
    }

    @Test
    public void stackPush2Pop2CheckBefore() {
        Stack stack = new Stack(100);


        stack.push(5);
        stack.push(10);



        assertThat(stack.pop(),is(10));

        assertThat(stack.pop(),is(5));

    }


    @Test
    public void stackPush2PeekTopCheckIfLastPushed() {
        Stack stack = new Stack(200);

        stack.push(10);
        stack.push(20);
        int lastPeek = stack.peek();
        assertThat(lastPeek,is(20));


    }


    @Test
    public void stackPush2PeekTopCheckIf2() {
        Stack stack = new Stack(100);

        stack.push(11);
        stack.push(12);
        int lastPeek = stack.peek();
        assertThat(stack.getStackSize(),is(2));

    }

    @Test
    public void stackPop1CheckException() {
        Stack stack = new Stack(100);

        stack.pop();
        assertThrows(IllegalStateException.class,() -> {stack.pop();});
    // I cant figure out if im using the assertThrows wrong or my code is wrong, am too rusty with programming.
    }

    @Test
    public void stackPeekTopCheckException() {
        Stack stack = new Stack(100);

        stack.peek();
        assertThrows(IllegalStateException.class,() -> {stack.pop();});
    }

}